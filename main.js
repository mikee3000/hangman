document.getElementById("gallows").innerHTML='<img src="assets/gallows_0.png"></img>';
String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}
function start_game(){
    document.getElementById("overlay").style.display = "none";
}

function finish_game(){
    // location.reload();
    document.getElementById("overlay").style.display = "block";
}
word = "hangman";
len_word = word.length;
var guessed_word = ""
var used = [];
for (i=0; i<len_word; i++) {
   guessed_word += '_ ';
}
var counter = 0;
document.getElementById("correct_letters").innerHTML  = '<span style="font-size:60px">'+guessed_word+'</span>';
function guess(){
    var character = document.getElementById("char").value.toLowerCase();
    document.getElementById("char").value = "";
    if (used.includes(character)) {
        document.getElementById("messages").innerText="Already used, please guess a different letter";
        return;
    }
    used.push(character);
    if (word.includes(character)){
        var tmp_word = word;
        var position = tmp_word.indexOf(character);
        while (position !== -1) {
            guessed_word = guessed_word.replaceAt(position*2, character);
            tmp_word = tmp_word.replaceAt(position, "_");
            counter += 1;
            position = tmp_word.indexOf(character);
        }

        document.getElementById("correct_letters").innerHTML  = '<span style="font-size:60px">'+guessed_word+'</span>';
        if (counter===len_word) {
            document.getElementById("messages").innerText="You WON!";
            // document.getElementById("char").disabled = true;
            return ;
        }
    } else {
        document.getElementById("gallows").innerHTML='<img src="' + corpse[0]+ '"></img>';
        var incorrect = document.getElementById("incorrect_letters").innerText;
        incorrect += character;
        document.getElementById("incorrect_letters").innerHTML  = '<span style="font-size:60px">'+incorrect+'</span>';
        corpse.shift();
        if (corpse.length === 0) {
                document.getElementById("messages").innerText="Game Over!";
                return ;
                // document.getElementById("char").attr.disabled = true;
        }
    }
}
var corpse = ["assets/gallows_1.png",
              "assets/gallows_2.png",
              "assets/gallows_3.png",
              "assets/gallows_4.png",
              "assets/gallows_5.png",
              "assets/gallows_6.png"
]
document.getElementById("messages").innerText="Please guess a letter";


var input = document.getElementById("char");

//// Execute a function when the user releases a key on the keyboard
//input.addEventListener("keyup", function(event) {
//    // Number 13 is the "Enter" key on the keyboard
//    if (event.key === "Enter") {
//        // Cancel the default action, if needed
//        event.preventDefault();
//        // Trigger the button element with a click
//        document.getElementById("submitBtn").click();
//    }
//});